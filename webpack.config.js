// Указываем основные переменные и плагины
var webpack = require('webpack');
var path = require('path');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

// Указываем рабочие директории
var build_dir = path.resolve(__dirname, 'project/build'); // Сгенерированные файлы
var source_dir = path.resolve(__dirname, 'project/source'); // Исходные файлы

// Создаем переменную с конфигами
var config = {
	entry: source_dir + '/main.jsx', // Основной файл проекта
	output: {
    path: build_dir, // Папка для сгенерированных файлов
    filename: 'generated.js' // Сгенерированный JS файл
  },
 	module : {
    loaders : [
      {
        test : /\.jsx?/, // Работаем с .jsx файлами
        include : source_dir,
        loader : 'babel'
      },
      {
      	test: /\.css$/, // Работаем с .css файлами
      	loader: ExtractTextPlugin.extract('style-loader', 'css-loader?minimize!postcss-loader') // Переносим стили в отдельный файл, минимизируем и добавляем префиксы для браузеров
      },
      {
        test: /\.(ico)$/, // Перемещаем favicon без изменений
        loader: 'static-loader'
      },
      {
        test: /\.(ttf|eot\?|svg|woff)$/, // Перемещаем шрифты
        loader: 'file-loader?name=fonts/[name].[ext]'
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('generated.css'), // Указываем куда попадут обработанные стили
    new HtmlWebpackPlugin({ // Генерируем HTML страницу по шаблону
      title: 'Home',
      inject: false,
      template: source_dir + '/template.ejs',
      mobile: true,
      minify: {
        collapseWhitespace: true
      }
    }),
    /*new webpack.DefinePlugin({ // Убираем предупреждения в версии 15
      "process.env": { 
        NODE_ENV: JSON.stringify("production") 
      }
    }),*/
    new webpack.NoErrorsPlugin(), // Убираем ошибки из вывода
    /*new webpack.optimize.UglifyJsPlugin({ // Сокращаем итоговый js файл
      compress: {
        warnings: false,
        drop_debugger: true,
        dead_code: true,
        unused: true
      },
      output: {
        comments: false,
        semicolons: true
      }
    })*/
  ],
  postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ] // Добавляем префиксы только для 2 последних версий браузеров
};

// Применяем конфиги
module.exports = config;

