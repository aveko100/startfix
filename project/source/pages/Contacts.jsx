import React, {Component} from 'react';
import Helmet from "react-helmet"; // Для изменения мета-данных

// Подключаем нужные компоненты
import ContactsData from '../components/Contacts/ContactsData.jsx';
import ContactsAdd from '../components/Contacts/ContactsAdd.jsx';

export default class Contacts extends Component {
  render() {
  	return (
      <div>
      	<Helmet title="Contacts" />
        <div className="block__header">Add contact</div>
        <ContactsAdd />
      	<h1>Contacts</h1>
      	<ContactsData />
      </div>
    );
  }
};