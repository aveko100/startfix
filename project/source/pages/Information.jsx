import React, {Component} from 'react';
import Helmet from "react-helmet"; // Для изменения мета-данных

// Подключаем нужные компоненты
import RemoteContent from '../components/Others/RemoteContent.jsx'; // Подгружаемый контент

export default class Information extends Component {
  	render() {
    	return (
    		<div>
	    		<Helmet title="Information" />
	    		<RemoteContent page="information" />
    		</div>
    	);
  	}
};