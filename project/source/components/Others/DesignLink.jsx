import React, {Component} from 'react';
import {Link, IndexLink} from 'react-router';

export default class DesignLink extends Component {
  	render() {
  		if (this.props.url == "/") return (
      		<IndexLink to={this.props.url} className="menu__button" activeClassName="menu__button-active">
      			<div className="button__text">{this.props.title}</div>
      		</IndexLink>
    	); else return (
    		<Link to={this.props.url} className="menu__button" activeClassName="menu__button-active">
    			<div className="button__text">{this.props.title}</div>
    		</Link>
    	);
  	}
};