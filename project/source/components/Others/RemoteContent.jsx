import React, {Component} from 'react';
var request = require('superagent'); // Подключаем аналог AJAX

export default class RemoteContent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: null, // Заголовок страницы
			content: null // Содержимое страницы
		};
		this.applyContent = this.applyContent.bind(this); // Разрешаем функции использовать this
	}

	componentDidMount() {
    request
      	.get(window.global.jsonUrl + "content.php") // Делаем запрос на сервер с контентом
      	.query({page: this.props.page}) // Отправляем идентификатор страницы
      	.end(this.applyContent); // Обрабатывать ответ будет функция applyContent
  	}

  	applyContent(error, result) {
  		if (error || !result.ok) { // Ошибка соединения
      		this.setState({
			    title: "Processing error", 
			    content: "There is a problem with the connection to the network."
			});
      	} else if (!result.body.hasOwnProperty("title")) { // Ошибка при обработке на сервере
      		this.setState({
			    title: "Processing error", 
			    content: result.body.error.trim()
			});
      	} else { // Данные получены в нужном виде
			this.setState({
		        title: result.body.title.trim(), 
		        content: result.body.content.trim()
		    });
      	}
  	}

  	render() {
	    return (
	    	<div>
	    		<h1>{this.state.title}</h1>
	        	<p>{this.state.content}</p>
	    	</div>
	    );
  	}
};