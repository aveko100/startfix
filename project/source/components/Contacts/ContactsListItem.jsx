import React, {Component} from 'react';

// Подключаем нужные модели
import ContactsModel from '../../models/ContactsModel.js'; // Модель контакта

export default class ContactsListItem extends Component { 
  render() {
    var entity = new ContactsModel(this.props.entity); // Преобразуем JSON в класс
    var email = "mailto:" + entity.email; // Представляем E-mail в виде ссылки

    return (
      <div className="block__contact">
        <div className="contact__container">
          <div className="container__box">
            <a href={email} className="box__href">
              <span className="box__name">{entity.name}</span>
            </a>
            <span className="box__phone">+7{entity.phone}</span>
          </div>
        </div>
      </div>
    );
  }
};