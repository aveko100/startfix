import React, {Component} from 'react';

// Подключаем нужные компоненты
import ContactsList from './ContactsList.jsx';

export default class ContactsData extends Component {
	constructor(props) {
		super(props);
		this.state = {
			contacts: [], // Переменная с контактами
			showContent: false, // Флаг видимости контактов
			error: null, // Текст ошибки при наличии
			socket: null // Глобальная переменная для WebSockets
		};
		this.onMessage = this.onMessage.bind(this); // Разрешаем функциям обращаться к state
		this.pushContact = this.pushContact.bind(this);
		this.onClose = this.onClose.bind(this);
	}

	componentDidMount() {
		try {
			this.state.socket = new WebSocket(window.global.webSocketServer); // Открываем соединение
			this.state.socket.onmessage = this.onMessage; // Обозначаем функцию, которая будет обрабатывать приходящие сообщения
			this.state.socket.onclose = this.onClose; // Обозначаем функция, которая будет вызывать при закрытии соединения
		} catch(error) {
			this.setState({
				error: error, // Неизвестная ошибка
				showContent: false // Контактов нет
			});
		}
	}

	componentWillUnmount() { // Переход к другой вкладке
		if (this.state.socket.readyState == WebSocket.OPEN) { // Возможно из-за плохого соединения соединение уже закрыто
			this.state.socket.onclose = function() {} // Не имеет смысла отображать контент на закрытой странице
			this.state.socket.close(); // Закрываем соединение
		}
	}

	onMessage(data) {
		var json = JSON.parse(data.data); // Превращаем JSON в массив

		if (json.length == 0) this.setState({
			error: "Database is empty", // Нет контактов в базе данных
			showContent: false // Контактов нет
		}); else {
			json.map(this.pushContact); // Добавляем каждый контакт в массив
			this.setState({
				error: null, // Ошибки нет
				showContent: true // Показываем весь контент
			});
		}
	}

	pushContact(contact) { this.state.contacts.unshift(contact); }

	onClose(data) {
		this.setState({
			error: "Irrelevant data", // Неактуальные данные
			showContent: true // Показываем подгруженные ранее контакты
		});
	}

	render() {
		if (this.state.error != null) var error = <div className="block__error">{this.state.error}</div>; // Присваиваем ошибке класс
		if (this.state.showContent) return (
			<div>
				{error}
				<ContactsList content={this.state.contacts} />
			</div>
		); else return (<div>{error}</div>);
	}
};