import React, {Component} from 'react';

// Подключаем нужные компоненты
import ContactsForm from './ContactsForm.jsx';
import ContactsInput from './ContactsInput.jsx';

export default class ContactsAdd extends Component {
  render() {
  	return (
    	<ContactsForm className="block__form">
            <ContactsInput 
            	name="name" 
            	validation="isName"
            	label="Enter contact name"
            	placeholder="Averin Konstantin"
            	error="Enter correct name of the contact" />
            <ContactsInput 
            	name="email" 
            	validation="isEmail"
            	label="Enter contact email"
            	placeholder="aveko100@gmail.com"
            	error="Enter correct email of the contact" />
            <ContactsInput 
            	name="phone" 
            	validation="isPhone"
            	label="Enter number without code"
            	placeholder="9852181294"
            	error="Enter correct phone number of the contact" />
        </ContactsForm>
    );
  }
};