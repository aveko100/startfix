import React, {Component} from 'react';
var classNames = require('classnames');

export default class ContactsInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value || '', // Введенный текст
      isValid: false, // Реальная валидность
      isVisibleValid: true, // Видимая валидность (для первоначально пустых полей)
      isFocused: false, // Фокус
      error: this.props.error // Ошибка
    };
    this.setValue = this.setValue.bind(this); // Разрешаем функциям использовать this
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  componentWillMount() {
    this.context.attachToForm(this); // Добавляем input в форму
  }

  componentWillUnMount() {
    this.context.detachFormForm(this); // Удаляем input из формы
  }

  onFocus() { this.setState({isFocused: true}); } // Фокус присутствует

  onBlur(event) { // Фокус отсутствует
    if (event.target.value === "") // Возвращаем label только тогда, когда введенный текст отсутствует
      this.setState({isFocused: false}); 
  }

  setValue(event) { // Функция для изменения значения поля
    this.setState({
      value: event.currentTarget.value, // Меняем state текущего компонента
      error: this.props.error // Возвращаем стандартный текст ошибки
    }, function () {
      this.context.validate(this); // Переадресовываем на функции валидации родительского компонента
    });
  }

  render() {
    var inputClasses = classNames({ // Классы стилей input-а
      "textinput__input": true,
      "textinput__input-place": this.state.isFocused // Показываем placeholder, когда в фокусе
    });
    var hrClasses = classNames({ // Классы стилей границы
      "textinput__hr-green": true,
      "textinput__hr-red": !this.state.isVisibleValid, // Красная линия при ошибке
      "textinput__hr-visible": this.state.isFocused // Зеленая линия при фокусе
    });
    var errorClasses = classNames({
      "textinput__error": true, 
      "visible": !this.state.isVisibleValid // Показываем ошибку при ее наличии
    });
    var labelClasses = classNames({
      "textinput__label": true,
      "textinput__label-error": !this.state.isVisibleValid && this.state.isFocused, // Красная надпись, когда ошибка и присутствует фокус
      "textinput__label-focus": this.state.isFocused // Зеленая надпись, когда в фокусе, но нет ошибки
    });
  	return (
      <div className="form__textinput">
        <label className={labelClasses} htmlFor={this.props.name}>{this.props.label}</label>
        <input 
          className={inputClasses} 
          value={this.state.value}
          type="text"
          id={this.props.name} 
          onFocus={this.onFocus} 
          onBlur={this.onBlur} 
          onChange={this.setValue}
          placeholder={this.props.placeholder} 
          autoComplete="off" />
        <hr className="textinput__hr-grey" />
        <hr className={hrClasses} />
        <div className={errorClasses}>{this.state.error}</div>
      </div>
    );
  }
};

ContactsInput.contextTypes = { // Объявляем функции
  attachToForm: React.PropTypes.func, // Добавление к форме
  detachFromForm: React.PropTypes.func, // Удаление из формы 
  validate: React.PropTypes.func // Валидация
};