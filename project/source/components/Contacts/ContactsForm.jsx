import React, {Component} from 'react';
import validator from 'validator';
var classNames = require('classnames');
var request = require('superagent');
var Promise = require("bluebird");

// Правила валидации
var rules = require('./ContactsValidations.js');

export default class ContactsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubmitting: false, // Флаг отправки данных на сервер для заморозки кноки
      isValid: false, // Валидность всех полей формы
      messageGrade: 0, // Цвет итогового сообщения
      message: null, // Итоговое сообщение
      key: 1 // Ключ для очистки формы
    };
    this.submit = this.submit.bind(this); // Разрешаем функциям использовать this
    this.updateModel = this.updateModel.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.validate = this.validate.bind(this);
  }

  componentWillMount() {
    this.inputs = []; // Массив компонентов входных строк
    this.model = {}; // Объект с данными из входных строк
  }

  getChildContext() { // Указываем соответствие функций и разрешаем им использовать this
    return {
      attachToForm: this.attachToForm.bind(this),
      detachFromForm: this.detachFromForm.bind(this),
      validate: this.validate
    }
  }

  attachToForm(component) {
    if (this.inputs.indexOf(component) === -1) { // Проверяем наличие
      this.inputs.push(component); // Добавляем в массив
    }
  }

  detachFromForm(component) {
    if (this.inputs.indexOf(component) !== -1) { // Проверяем наличие
      this.inputs = this.inputs.slice(0, componentPos).concat(this.inputs.slice(componentPos + 1)); // Удаляем из массива
    }
  }

  updateModel() { // Обновляем объект со значениями input-ов
    Object.keys(this.inputs).forEach(function (name) { // Проходимся по массиву
      this.model[this.inputs[name].props.name] = this.inputs[name].state.value; // Добавляем значение каждого поля в модель
    }.bind(this));
  }

  submit(event) { // Отправляем данные на сервер
    event.preventDefault(); // Предотвращаем стандартное исполнение
    this.setState({
      isSubmitting: true
    });
    this.updateModel(); // Обновляем данные
    request
      .get(window.global.jsonUrl + "add.php")
      .query(this.model)
      .end(this.checkResponse.bind(this));
  }

  checkResponse(error, result) {
    if (error || !result.ok || result.body.success == 0) { // Ошибка с интернетом
      this.setState({
        messageGrade: 1,
        message: "Try again"
      });
    } else if (result.body.success == 1) { // Ошибка с разбором данных на сервере
      Object.keys(result.body.errors).forEach(function (error) { // Проходим по массиву ошибок
        Object.keys(this.inputs).forEach(function (name) { // Проходимся по массиву input-ов
          if (this.inputs[name].props.name == error) {
            this.inputs[name].setState({
              isValid: false,
              isVisibleValid: false,
              error: result.body.errors[error]
            });
          }
        }.bind(this));
      }.bind(this));  
      this.setState({isValid: false}); 
    } else if (result.body.success == 2) { // Телефон присутствует в базе
      this.setState({
        messageGrade: 1,
        message: "Coincidence",
      });
    } else if (result.body.success == 3) { // Нет ошибок, контакт добавлен
      this.setState({
        messageGrade: 2,
        message: "Success",
        isValid: false,
        key: this.state.key + 1
      });
    }
    this.setState({isSubmitting: false}); 
  }

  validate(component) {
    if (!component.props.validation) return;
    var isValid = true;
    if (!component.state.value || !rules[component.props.validation](component.state.value)) isValid = false; // Проверяем соответствие условиям валидации
    if (component.props.name == "phone" && isValid) { // Будем выполнять данную валидацию только для поля с телефоном
      this.checkPhone(component.state.value).then(this.onResolved.bind(this)); // Указываем функцию, которая будет выполняться при ошибке
    }
    component.setState({ // Обновляем валидность полей
      isValid: isValid,
      isVisibleValid: isValid
    }, this.validateForm);
  }

  checkPhone(phone) {
    return new Promise (function (resolve, reject) {
      request
        .get(window.global.jsonUrl + "check.php")
        .query({phone: phone}) // Отправляем номер телефона
        .end(function(error, result) { // Обрабатываем полученный результат
          if (error || !result.ok || !result.body.success) resolve(); // Нельзя добавлять данный телефон
        });
    });
  }

  onResolved() { // Телефон существует в базе
    Object.keys(this.inputs).forEach(function (name) { // Проходимся по массиву input-ов
      if (this.inputs[name].props.name == "phone") { // Находим input с телефоном
        this.inputs[name].setState({ // Выводим ошибку
          isValid: false,
          isVisibleValid: false,
          error: "Coincidence or mistake"
        });
      }
    }.bind(this));
    this.validateForm(); // Отключаем кнопку отправки запроса
  }

  validateForm() { // Обновляем валидность всей формы
    var allIsValid = true;
    var inputs = this.inputs;
    Object.keys(inputs).forEach(function (name) { // Проходимся по всем полям
      if (!inputs[name].state.isValid) allIsValid = false; // Хоть одно поле невалидно, вся форма невалидна
    });
    this.setState({
      isValid: allIsValid
    });
  }

  render() {
    var submitClasses = classNames({ // Классы стилей кнопки отправки данных
      "form__submit-container": true,
      "form__submit-container-disabled": this.state.isSubmitting || !this.state.isValid // Отключаем кнопку, если хоть одно поле невалидно или происходит отправка валидных данных
    });
    var messageClasses = classNames({ // Классы стилей сообщения об ошибке
      "form__message": true,
      "form__message-ok": this.state.messageGrade == 2,
      "form__message-visible":  this.state.messageGrade > 0
    });
  	return (
    		<form onSubmit={this.submit} className={this.props.className} key={this.state.key}>
          {this.props.children}
          <div className={submitClasses}>
            <button className="submit-div__button" type="submit" disabled={this.state.isSubmitting || !this.state.isValid}>
              <div className="button__animation">
                <span className="animation__text">Add information</span>
              </div>
            </button>
          </div>
          <div className={messageClasses}>{this.state.message}</div>
        </form>
    );
  }
};

ContactsForm.childContextTypes = { // Объявляем функции, которые будут использовать input-ы
  attachToForm: React.PropTypes.func, // Добавление к форме
  detachFromForm: React.PropTypes.func, // Удаление из формы
  validate: React.PropTypes.func // Валидация
};