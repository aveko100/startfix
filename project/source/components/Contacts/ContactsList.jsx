import React, {Component} from 'react';

// Подключаем нужные компоненты
import ContactsListItem from './ContactsListItem.jsx'; // Дизайн контакта

export default class ContactsList extends Component { 
  render() {
  	var contacts = this.props.content.map(function(contact, key) { // Проходим по всем контактам
  		return(<ContactsListItem key={key} entity={contact} />);
  	});

    return (
      <div>{contacts}</div>
    );
  }
};