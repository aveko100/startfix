// Подключаем библиотеки
import React, {Component} from 'react';
import Helmet from "react-helmet";
import {render} from 'react-dom';
import {Router, Route, IndexRoute, browserHistory} from 'react-router';
var classNames = require('classnames');
var BodyClassName = require('react-body-classname');

// Прописываем статические объекты для переноса
require("static?!./favicon.ico?output=favicon.ico");

// Подключаем стили
import './main.css';

// Подключаем страницы
import Home from './pages/Home.jsx'; // Главная страница
import Contacts from './pages/Contacts.jsx'; // Страница контактов
import Information from './pages/Information.jsx'; // Страница с информацией
import Help from './pages/Help.jsx'; // Вспомогательная страница
import NotFound from './pages/NotFound.jsx'; // Не найдено

// Подключаем нужные компоненты
import DesignLink from './components/Others/DesignLink.jsx'; // Ссылка

// Работаем с основным шаблоном страницы
class Template extends Component {
  constructor(props) {
    super(props);
    this.state = {clicked: false}; // Клик по кнопке "открыть меню" в мобильной версии
    this.activeMobile = this.activeMobile.bind(this); // Позволяем функциям использовать this
    this.disableMobile = this.disableMobile.bind(this);
  }

  activeMobile() { this.setState({clicked: true}); } // Кнопка была нажата
  disableMobile() { this.setState({clicked: false}); } // Меню закрыто

  render() {
    var backgroundClasses = classNames({ // Классы для потемнения при открытии меню из мобильной версии
      "background": true,
      "background-active": this.state.clicked
    });

    var sidebarClasses = classNames({ // Классы для вывода сайдбара в мобильной версии
      "sidebar": true,
      "sidebar-active": this.state.clicked
    });

    var bodyClasses = classNames({"body-hidden": this.state.clicked}); // Класс для удаления скролла при открытии меню

    return (
      <BodyClassName className={bodyClasses}>
        <div>
          <div className="header">
            <div className="header__mobile" onClick={this.activeMobile}>
              <svg className="mobile__svg" viewBox="0 0 24 24">
                <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path>
              </svg>
            </div>
            <div className="header__title">Startfix</div>
          </div>
          <div className="container">
            <div className="container__margins">
              <div className="margins__block">{this.props.children}</div>
            </div>
          </div>
          <div className={backgroundClasses} onClick={this.disableMobile}></div>
          <div className={sidebarClasses}>
            <div className="sidebar__title">Menu</div>
            <div className="sidebar__menu"><Menu /></div>
          </div>
        </div>
      </BodyClassName>
    );
  }
};

// Генерируем ссылки
class Menu extends Component {
  render() {
    return (
      <div>
        <DesignLink title="Home" url="/" />
        <DesignLink title="Contacts" url="/contacts/" />
        <DesignLink title="Information" url="/information/" />
        <DesignLink title="Help" url="/help/" />
      </div>
    );
  }
};

// Глобальные переменные
class GlobalClass {
  constructor() {
    this.webSocketServer = "ws://78.107.234.99:7777/"; // Адрес сервера WebSockets
    this.jsonUrl = "http://78.107.234.99/api/"; // Адрес контента в формате JSON
  }
}

// Применяем глобальные переменные
window.global = new GlobalClass();

// Прописываем структуру сайта
render((
  <Router history={browserHistory}>
    <Route path="/" component={Template}>
      <IndexRoute component={Home} />
      <Route path="contacts" component={Contacts} />
      <Route path="information" component={Information} />
      <Route path="help" component={Help} />
      <Route path="*" component={NotFound} />
    </Route>
  </Router>
), document.getElementById('content'));