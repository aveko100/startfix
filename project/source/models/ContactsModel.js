export default class ContactsModel {
	constructor(array) {
		this.name = array.name;
		this.email = array.email;
		this.phone = array.phone;
	}	
}